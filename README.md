# README #

Odaesas in a first concept of a civilization in which women have more control in society. The rules to start are simple: 

* Only women are allowed to trade with a form of currency (coin). Men are only allowed to trade items they have produced or farmed themselves.  
* Women cannot be enslaved by men. 

## Location and Culture ##

Whilst there is no strict rule, I'm intrigued by the Korean culture, to which I have ancestral links. I'll try to create an environment based on images of Odaesan National Park and cultural events. 

## Factions ##

There will be three types of factions: 

* Rulers and Warriors: This is a group of women who has sworn to protect the rules and society. 
* Guild Houses: Any house led by a woman is a Guild House and allowed to trade using coin. Men and women can join these houses if they accept their rules. Even a single independent woman is considered a guild.
* Makers Houses: Like women, men can create houses with their own rules. Only a house led by men cannot accept women to join the house and they cannot trade with coin, but only direct exchanges.

## Legal System ##

### Captital Rules ###

The legal system has only two captial rules, as their name implies, violating them will result in immediate captial punishment. These are: 

* Any child, boy or adult man must not give away a product he has not grown or created with his own hands, unless he does so at the order of a guilded woman. 
* Any child, boy or adult man must not accept as payment of a trade the lifetime of a girl or adult woman or a part thereof. 

### Captial Intention ###

The rules of the land are short. An unwritten rule, enforced as strictly as the captial rules, is called the 'Capital Itention'. 
Any action that is interpreted as undermining the Captial Rules, can be seen as Capital Intention. 
If you harm a Ruler or Warrior, you face no punishment. But if you are considered to have done so, because you intended to prevent a capital rule to be enforced, no other than the capital punishment itself is to be applied. 